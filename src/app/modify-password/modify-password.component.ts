import { Component, OnInit,Input } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { ElementRef } from '@angular/core';

//own
import { GlobalService } from '../common/service/global.service';
import { ServerService } from '../common/service/server.service';

@Component({
  selector: 'app-modify-password',
  templateUrl: './modify-password.component.html',
  styleUrls: ['./modify-password.component.scss']
})
export class ModifyPasswordComponent implements OnInit, OnDestroy {
	@Input() quit = 'none';
	onquit(v){
		console.log(v)
		this.quit = v;
	}
  constructor(private er : ElementRef, private globalService: GlobalService, private serverService : ServerService) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    //do nothing
  }
  show(e) {
    let div = e.target.parentNode;
    let l = div.children.length - 1;
    div = div.children[l];
    if (div.innerHTML) {
      div.style.visibility = 'visible';
    }
  }
  hidden(e) {
    let div = e.target.parentNode;
    let l = div.children.length - 1;
    div.children[l].style.visibility = 'hidden';
    e.target.focus();
  }
  hiddenSelf(e) {
    e.target.style.visibility = 'hidden';
    e.target.parentNode.children[1].focus();
  }
  valueChange(e) {
    let div = e.target.parentNode;
    let s = e.target.placeholder + '!';
    let l = div.children.length - 1;
    div = div.children[l];
    if (e.target.value) {
      div.innerHTML = '';
    } else {
      div.innerHTML = s;
    }
  }
  submit(e) {
    
  }
}
