import { Component, OnInit,Input } from '@angular/core';
import {Http}    from '@angular/http';
import "rxjs/Rx";
import {Observable} from "rxjs";

import { GlobalService } from '../common/service/global.service';
import * as moment from 'moment';

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
	@Input() content:string;
	_startDate = null;
  	_endDate = null;
  	newArray = (len) => {
	    const result = [];
	    for (let i = 0; i < len; i++) {
	      result.push(i);
	    }
	    return result;
	};
	_startValueChange = () => {
	    if (this._startDate > this._endDate) {
	      this._endDate = null;
	    }
	};
	_endValueChange = () => {
	    if (this._startDate > this._endDate) {
	      this._startDate = null;
	    }
	};
	_disabledStartDate = (startValue) => {
	    if (!startValue || !this._endDate) {
	      return false;
	    }
	    return startValue.getTime() >= this._endDate.getTime();
	};
	_disabledEndDate = (endValue) => {
	    if (!endValue || !this._startDate) {
	      return false;
	    }
	    return endValue.getTime() <= this._startDate.getTime();
	};
	get _isSameDay() {
	    return this._startDate && this._endDate && moment(this._startDate).isSame(this._endDate, 'day')
	}
	get _endTime() {
	    return {
	      nzHideDisabledOptions: true,
	      nzDisabledHours: () => {
	        return this._isSameDay ? this.newArray(this._startDate.getHours()) : [];
	      },
	      nzDisabledMinutes: (h) => {
	        if (this._isSameDay && h === this._startDate.getHours()) {
	          return this.newArray(this._startDate.getMinutes());
	        }
	        return [];
	      },
	      nzDisabledSeconds: (h, m) => {
	        if (this._isSameDay && h === this._startDate.getHours() && m === this._startDate.getMinutes()) {
	          return this.newArray(this._startDate.getSeconds());
	        }
	        return [];
	      }
	    }
	}
	onClickEmitter(e) {
		this.closeAllOption();
	}
	
	closeAllOption() {
		let li, div;
		for (let i = 0, l = this.divArray.length; i < l; i++) {
			li = this.divArray[i];
			div = li.getElementsByClassName('m-option')[0];
			div.style.display = 'none';
			li.getElementsByTagName('img')[0].style.transform = 'rotate(0deg)';
		}
		this.divArray.length = 0;
	}
	emptyAllOption() {
		let nodes = document.getElementsByClassName('m-checked');
		console.log(nodes)
		if(nodes.length>0){
			Array.from(nodes).forEach(function(v){
				v.classList.remove("m-checked");
			})
		}		
	}
	divArray = [];
  
	onOptionClick(event) {
		event.stopPropagation();
		
		let li = event.target;
		if ('IMG' === li.nodeName.toUpperCase()) {
			li = li.parentNode;
		}
		if ('LI' != li.nodeName.toUpperCase()) {
			return;
		}
		let img = li.getElementsByTagName('img')[0];
		let div = li.getElementsByClassName('m-option')[0];
		let display = div.style.display;
		this.closeAllOption();
		if(display != 'block'){
			div.style.display = 'block';
			img.style.transform = 'rotate(180deg)';
			this.divArray.push(li);
		}else{
		}
	}
	onCheckClick(event) {
		event.stopPropagation();
		console.log(event.target);
		
		if(event.target.tagName == 'SPAN'){
			let classList = event.target.classList;
			let div = event.target.parentNode.parentNode;
			for (let i = 0, a = div.children, l = a.length; i < l; i++) {
				a[i].children[0].classList.remove("m-checked");
			}
			if(event.target.classList.contains('m-check')){
				event.target.classList.add("m-checked");
			}else{
				event.target.classList.remove("m-checked");
			}
		}else{
			let classList = event.target.classList;
			let div = event.target.parentNode;
			for (let i = 0, a = div.children, l = a.length; i < l; i++) {
				a[i].classList.remove("m-checked");
			}
			if(event.target.className == ''){
				event.target.classList.add("m-checked");
			}else{
				event.target.classList.remove("m-checked");
			}
		}
	}
	constructor(public http:Http, private globalService: GlobalService) {

	}
	

	ngOnInit() {
		this.globalService.html_body_click_handle.push(() => {this.closeAllOption();});
		this.emptyAllOption();
		this._startDate = null;
  		this._endDate = null;
	}

	submit(e) {
		let dataSource = this.http.get(this.globalService.data_url).map(response => response.json());
		dataSource.subscribe(
			data => {
				this.globalService.item_list.id++;
				this.globalService.item_list.data = data;
			}
		);
	}
	reset(e){
		this.emptyAllOption();
		this.globalService.item_list.id++;
		this.globalService.item_list.data = null;
		this._startDate = null;
  		this._endDate = null;
	}
}

