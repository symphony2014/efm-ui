//@angular
import { Injectable } from '@angular/core';

//all the remote request via this class instance's method, this is a stub function
@Injectable()
export class ServerService {
	
	//name is username, key is password, return a promise object
	//if name and key is right then return session id else throws an error
	login(name, key) {
		let p = new Promise((resolve, reject) => {
			if (name === 'error') {
				reject({
					username : 'user name is error',
					password : null
				});
			}
			if (key === 'error') {
				reject({
					username : null,
					password : 'password is error'
				});
			}
			let id = (new Date()).getTime();
			resolve(id);
		});
		return p;
	}
}
