import { Injectable } from '@angular/core';

let id = 1;

@Injectable()
export class GlobalService {
	
	userInfo = null;
	item_list = {
		id : 0,
		s : null,
		data : null
	};
	data_url = 'assets/data/data.json';
	html_body_click_handle = [];
	get_id() {
		return id++;
	}
}
