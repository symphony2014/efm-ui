import { Component, OnInit } from '@angular/core';
import { OnDestroy, ElementRef } from '@angular/core';
import {Http}    from '@angular/http';
import "rxjs/Rx";
import {Observable} from "rxjs";

import { GlobalService } from '../common/service/global.service';

@Component({
  selector: 'app-trend-analysis',
  templateUrl: './trend-analysis.component.html',
  styleUrls: ['./trend-analysis.component.scss']
})
export class TrendAnalysisComponent implements OnInit, OnDestroy {

  item_list = {
		id : 0,
		data : null
	};
	row = 8;
	table:Object[];
	private company:Object[];
    constructor(public http:Http, private globalService: GlobalService, private ef : ElementRef) { }

  ngOnInit() {
  		this.globalService.data_url = 'assets/data/analysis.json';
		this.globalService.item_list.id = this.item_list.id;
		this.globalService.item_list.s = this;
  }
  ngOnDestroy() {
		console.log('destory');
  }
  ngDoCheck() {
		if (this.globalService.item_list.id !== this.item_list.id && this.globalService.item_list.s === this) {
			this.item_list.id = this.globalService.item_list.id;
			this.item_list.data = this.globalService.item_list.data;
			let set_data = data => {
				this.table = data.data.table;
				this.company = data.data.company;
				console.log(this.table)
			}
			console.log(this.item_list.data)
			if(this.item_list.data){
				set_data(this.item_list.data);
			}
			
		}
	}

}
