import { Component, OnInit ,Output,EventEmitter} from '@angular/core';

import { GlobalService } from '../common/service/global.service';
import { NzModalService } from 'ng-zorro-antd';

@Component({
	selector: 'app-head',
	templateUrl: './head.component.html',
	styleUrls: ['./head.component.scss']
})
export class HeadComponent implements OnInit {
	nav_active1: boolean = true;
	nav_active2: boolean = false;
	nav_active3: boolean = false;
	@Output() quitone = new EventEmitter();
	@Output() title = new EventEmitter();
	showConfirm = () => {
		console.log('dddd')
		this.quitone.emit('block');
	}

	onMenuClick(event,num){
		event.preventDefault();
		event.stopPropagation();
		this.nav_active1 = false;
		this.nav_active2 = false;
		this.nav_active3 = false;
		this['nav_active' + num] = true;
		console.log(event.target)
		let div = event.target.children[0];
		if(div){
			if(div.style.display != 'block'){
				div.style.display = 'block';
			}else{
				div.style.display = 'none';
			}
		}
		if(num != 3){
			this.closeOption();
			if(this.divArray[0] && this.divArray[0].getElementsByClassName('nav-active')[0]){
				this.divArray[0].getElementsByClassName('nav-active')[0].classList.remove("nav-active");
			}
			
		}else{
			this.divArray = [];
			this.divArray.push(div);
		}
		event.target.parentNode.click();
	}
	divArray = [];
	oldArray = [];
	onOption(event){
		event.preventDefault();
		event.stopPropagation();
		this.divArray = [];
		let nodes = document.getElementsByClassName('nav-option');
		console.log(nodes)
		if(nodes.length>0){
			Array.from(nodes).forEach(function(v){
				v.classList.remove("nav-active");
			})
		}		
		event.target.classList.add("nav-active");
		event.target.parentNode.parentNode.style.display = 'none';
		this.divArray.push(event.target.parentNode.parentNode);
		event.target.parentNode.click();
	}
	constructor(private globalService: GlobalService,private confirmServ: NzModalService) { }
	closeOption(){
		if(this.divArray.length>0){
			this.divArray[0].style.display = 'none';
		}
		
	}
	ngOnInit() {
		//this.globalService.html_body_click_handle.push(() => {this.closeOption();});
	}
}
