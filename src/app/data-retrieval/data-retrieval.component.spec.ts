import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataRetrievalComponent } from './data-retrieval.component';

describe('DataRetrievalComponent', () => {
  let component: DataRetrievalComponent;
  let fixture: ComponentFixture<DataRetrievalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataRetrievalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataRetrievalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
