import { Component, OnInit } from '@angular/core';
import { OnDestroy, ElementRef } from '@angular/core';
import {Http}    from '@angular/http';
import "rxjs/Rx";
import {Observable} from "rxjs";


import {echarts} from "echarts/dist/echarts";

import { GlobalService } from '../common/service/global.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

	item_list = {
		id : 0,
		data : null
	};
	
	chartOption = {
		title: {
			text: 'ECharts 入门示例'
		},
		tooltip: {},
		legend: {
			data:['销量']
		},
		xAxis: {
			data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
		},
		yAxis: {},
		series: [{
			name: '销量',
			type: 'bar',
			data: [5, 20, 36, 10, 10, 20]
		}]
	}

	number:Object[];
	private degree:Array<object>;
	private company:Object[];
	dataSource: Observable<any>;
  
	constructor(public http:Http, private globalService: GlobalService, private ef : ElementRef) {
	}

	ngOnInit() {
		this.globalService.data_url = 'assets/data/data.json';
		this.globalService.item_list.id = this.item_list.id
		this.globalService.item_list.s = this;
		
		
		
		/*
		var myChart = echarts.init(document.getElementById('echarts_div'));

        // 指定图表的配置项和数据
        var option = {
            
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);*/
	}
	ngOnDestroy() {
		console.log('destory');
	}
	
	ngDoCheck() {
		if (this.globalService.item_list.id !== this.item_list.id && this.globalService.item_list.s === this) {
			this.item_list.id = this.globalService.item_list.id;
			this.item_list.data = this.globalService.item_list.data;
			let set_data = data => {
				this.number = data.data.number;
				this.degree = data.data.degree;
				this.company = data.data.company;
			}
			console.log(this.item_list.data)
			if(this.item_list.data){
				set_data(this.item_list.data);
			}
			
		}
	}
	delete(e){
		e.target.parentNode.parentNode.style.display = 'none';
	}
}
