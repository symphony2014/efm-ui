import { Component, OnInit,Input } from '@angular/core';

import { EventEmitter } from '@angular/core';

import { GlobalService } from '../common/service/global.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
	@Input() quit = 'none';
	@Input() title = '华丽佳和';
	onquit(v){
		console.log(v)
		this.quit = v;
	}
	ontitle(v){
		this.title = v;
	}
	constructor(private globalService: GlobalService) { }

	ngOnInit() {
		
	}
	
	clickEmitter = new EventEmitter();
	
	html_body_click(e) {
		this.clickEmitter.emit(e);
		let hs = this.globalService.html_body_click_handle;
		let l = hs.length;
		for (let i = 0; i < l; i++) {
			hs[i](e);
		}
	}
}
