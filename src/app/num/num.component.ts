import { Component, OnInit,Input } from '@angular/core';

import { GlobalService } from '../common/service/global.service';

@Component({
  selector: 'app-num',
  templateUrl: './num.component.html',
  styleUrls: ['./num.component.scss']
})
export class NumComponent implements OnInit {
	@Input() company;
	item_list = null;
	com = [];
	constructor(private globalService: GlobalService) { }

	ngOnInit() {
		this.com = [];
		for(let i = 0;i<this.company.length;i++){
			if(i>0){
				this.com.push(this.company[i]);
			}
		}
	}
	
	ngDoCheck() {
	}
	changeCom(e,i){
		let li = document.getElementsByClassName('m-num')[0].children;
		console.log(li);
		Array.from(li).forEach(function(v){
			v.classList.remove("m-munActive");
		})
		li[i].classList.add("m-munActive");
	}
}
