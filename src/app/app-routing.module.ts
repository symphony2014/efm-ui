import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";

import {HomeComponent} from "./home/home.component";
import {YujingComponent} from "./yujing/yujing.component";
import {LoginComponent} from "./login/login.component";
import {IndexComponent} from "./index/index.component";
import { ModifyPasswordComponent } from './modify-password/modify-password.component';
import { BusinessUnitComponent } from './business-unit/business-unit.component';
import { DataRetrievalComponent } from './data-retrieval/data-retrieval.component';
import { SingleRankingComponent } from './single-ranking/single-ranking.component';
import { TrendAnalysisComponent } from './trend-analysis/trend-analysis.component';
import { ExecutiveSummaryComponent } from './executive-summary/executive-summary.component';

const routes: Routes = [
	{path:'',redirectTo: '/login',pathMatch:'full'},
	{path:'login',component: LoginComponent},
	{path:'index',component: IndexComponent,children:[
		{path:'',redirectTo: '/index/home',pathMatch:'full'},
		{path:'home',component: HomeComponent},
		{path:'yujing',component: YujingComponent},
		{path:'businessunit',component: BusinessUnitComponent},
		{path:'dataretrieval',component: DataRetrievalComponent},
		{path:'singleranking',component: SingleRankingComponent},
		{path:'analysis',component: TrendAnalysisComponent},
		{path:'executive',component: ExecutiveSummaryComponent}
	]},
	{path:'password',component: ModifyPasswordComponent},
	{path:'**',component: HomeComponent}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }