import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-quit',
  templateUrl: './quit.component.html',
  styleUrls: ['./quit.component.scss']
})
export class QuitComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  @Output() quitone = new EventEmitter();
	onhide(e){
		if(e.target.classList.contains('mask') || e.target.classList.contains('mask-no')){
			e.stopPropagation();
			console.log('dddddd')
			this.quitone.emit('none');
		}	
	}
	tologin(e){
		e.stopPropagation();
		this.quitone.emit('none');
		this.router.navigate(['/login']);
	}
}
