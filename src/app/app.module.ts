import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import {HttpModule}    from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeadComponent } from './head/head.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './home/home.component';
import { YujingComponent } from './yujing/yujing.component';


import { GlobalService } from './common/service/global.service';
import { ServerService } from './common/service/server.service';
import { NumComponent } from './num/num.component';
import { LoginComponent } from './login/login.component';
import { IndexComponent } from './index/index.component';
import { ModifyPasswordComponent } from './modify-password/modify-password.component';
import { QuitComponent } from './quit/quit.component';
import { BusinessUnitComponent } from './business-unit/business-unit.component';
import { DataRetrievalComponent } from './data-retrieval/data-retrieval.component';
import { SingleRankingComponent } from './single-ranking/single-ranking.component';
import { TrendAnalysisComponent } from './trend-analysis/trend-analysis.component';


import { AngularEchartsModule } from 'ngx-echarts';
import { ExecutiveSummaryComponent } from './executive-summary/executive-summary.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadComponent,
    FooterComponent,
    MainComponent,
    HomeComponent,
    YujingComponent,
    NumComponent,
    LoginComponent,
    IndexComponent,
    ModifyPasswordComponent,
    QuitComponent,
    BusinessUnitComponent,
    DataRetrievalComponent,
    SingleRankingComponent,
    TrendAnalysisComponent,
    ExecutiveSummaryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgZorroAntdModule.forRoot(),
	AngularEchartsModule
  ],
	providers: [
	  	GlobalService,
		ServerService
	],
  bootstrap: [AppComponent]
})
export class AppModule { }
