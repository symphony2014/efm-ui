import { Component, OnInit,Input } from '@angular/core';
import {Http}    from '@angular/http';
import "rxjs/Rx";
import {Observable} from "rxjs";

import { GlobalService } from '../common/service/global.service';

@Component({
	selector: 'app-yujing',
	templateUrl: './yujing.component.html',
	styleUrls: ['./yujing.component.scss']
})
export class YujingComponent implements OnInit {
	item_list = {
		id : 0,
		data : null
	};
	warningSituation:Object[];
	private city:Object[];
	private company:Object[];
	
	dataSource: Observable<any>;
	
	constructor(public http:Http, private globalService: GlobalService) {
	}

	ngOnInit() {
		this.globalService.data_url = 'assets/data/yujing.json';
		this.globalService.item_list.id = this.item_list.id;
		this.globalService.item_list.s = this;
	}
	ngDoCheck() {
		if (this.globalService.item_list.id !== this.item_list.id && this.globalService.item_list.s === this) {
			this.item_list.id = this.globalService.item_list.id;
			this.item_list.data = this.globalService.item_list.data;
			let set_data = data => {
				this.warningSituation = data.data.warningSituation;
				this.city = data.data.city;
				this.company = data.data.company;
			}
			if(this.item_list.data){
				set_data(this.item_list.data);
			}
		}
	}
}
