import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YujingComponent } from './yujing.component';

describe('YujingComponent', () => {
  let component: YujingComponent;
  let fixture: ComponentFixture<YujingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YujingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YujingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
