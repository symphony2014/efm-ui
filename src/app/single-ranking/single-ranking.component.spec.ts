import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRankingComponent } from './single-ranking.component';

describe('SingleRankingComponent', () => {
  let component: SingleRankingComponent;
  let fixture: ComponentFixture<SingleRankingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
