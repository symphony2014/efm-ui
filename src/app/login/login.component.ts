//@angular
import { Component } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core';
import { ElementRef } from '@angular/core';

//own
import { GlobalService } from '../common/service/global.service';
import { ServerService } from '../common/service/server.service';

let verifyCodeArray = [
	'66er',
	'7b9r',
	's59r'
]


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
	
	verifyCode = 1;
	verifyCodeSrc = '../../assets/identifying-code/1.png';

	constructor(private er : ElementRef, private globalService: GlobalService, private serverService : ServerService) {}

	ngOnInit() {
		//do nothing
		this.changeVerifyCode();
	}

	ngOnDestroy() {
		//do nothing
	}

	//inter method
	changeVerifyCode() {
		this.verifyCode = Number.parseInt(`${(Math.random() * 3)}` ) + 1;
		//this.verifyCode =2;
		this.verifyCodeSrc = `../../assets/identifying-code/${new String(this.verifyCode)}.png`;
	}

	show(e) {
		let div = e.target.parentNode;
		let l = div.children.length - 1;
		div = div.children[l];
		if (div.innerHTML) {
			div.style.visibility = 'visible';
		}
	}
	hiddenSelf(e) {
		e.target.style.visibility = 'hidden';
		e.target.parentNode.children[1].focus();
	}
	hidden(e) {
		let div = e.target.parentNode;
		let l = div.children.length - 1;
		div.children[l].style.visibility = 'hidden';
		e.target.focus();
	}
	valueChange(e) {
		let div = e.target.parentNode;
		let s = e.target.placeholder + '!';
		let l = div.children.length - 1;
		div = div.children[l];
		if (e.target.value) {
			div.innerHTML = '';
		} else {
			div.innerHTML = s;
		}
	}

	//bind in the template
	login(e) {
		let g = this.globalService;
		let isNull = false;
		let div = this.er.nativeElement.children[0].children[0];
		let set_error = (index, message) => {
			let element = div.children[index];
			//element.children[1].focus();
			let l = element.children.length - 1;
			element.children[l].innerHTML = message + '&nbsp;';
			element.children[l].style.visibility = 'visible';
			isNull = true;
		}
		let vc = div.children[3].children[1].value;
		if (!vc) {
			set_error(3, '请输入验证码！');
		}
		let username = div.children[1].children[1].value;
		let password = div.children[2].children[1].value;
		if (!password) {
			set_error(2, '请输入密码！');
		}
		if (!username) {
			set_error(1, '请输入用户名！');
		}
		if (isNull) {
			this.changeVerifyCode();
			return;
		}
		if (vc.toLowerCase() === verifyCodeArray[Number.parseInt(this.verifyCode - 1 + '')]) {
			let ss = this.serverService;
			let w = ss.login(username, password);
			w.then((s) => {
				if (!g.userInfo) {
					g.userInfo = {};
				}
				if (g.userInfo) {
					g.userInfo.username = username;
					g.userInfo.password = password;
					g.userInfo.session = s;
				}
				window.location.pathname = '/index';
			}).catch((err) => {
				if (err.username) {
					set_error(1, err.username);
					this.changeVerifyCode();
				} else {
					set_error(2, err.password);
					this.changeVerifyCode();
				}
			});
		} else {
			set_error(3, '验证码错误!');
			this.changeVerifyCode();
		}
	}
}
